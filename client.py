import datetime
import typing
from dataclasses import dataclass

import aiohttp.client

from . import errors


class RiptesterClient:
    VERSION = 1

    client: aiohttp.client.ClientSession

    def __init__(self, token: str, *, client: aiohttp.ClientSession = None, base_url: str = ...):
        self.token = token
        if client:
            self.client = client
            self._needs_init = False
        else:
            self.client = aiohttp.ClientSession()
            self._needs_init = True
        if base_url is Ellipsis:
            self.base_url = "http://localhost:8080"

    async def __aenter__(self):
        if self._needs_init:
            await self.client.__aenter__()
        return self

    async def __aexit__(self, *args, **kwargs):
        if self._needs_init:
            return await self.client.__aexit__(*args, **kwargs)

    async def run_code(self, language: str, code: bytes, stdin: bytes, environ: list[bytes]):
        assert not any(b"\0" in entry for entry in environ), "Environment variables cannot contain NULs"
        async with self.client.post(
                self.url(f"exec/{language}"),
                data=aiohttp.FormData(
                    {
                        "code": code.decode("latin-1"),
                        "stdin": stdin.decode("latin-1"),
                        "environ": b"\0".join(environ).decode("latin-1")
                    },
                    charset="latin-1"
                ),
                headers={"X-Api-Token": f"Bearer {self.token}"}
        ) as resp:
            if resp.status >= 400:
                raise errors.error_from_json(await resp.json())
            return ExecutionResult(await resp.json())

    def url(self, url):
        return self.base_url + f"/v{self.VERSION}/" + url


@dataclass()
class ExecutionResult:
    stdout: bytes
    stderr: bytes
    rc: int
    runtime: datetime.timedelta

    def __init__(self, data: dict[str, typing.Union[str, int]]):
        self.stdout = data["stdout"].encode("latin-1")
        self.stderr = data["stderr"].encode("latin-1")
        self.rc = data["rc"]
        self.runtime = datetime.timedelta(seconds=data["runtime"])
